package com.company.bookseller.Controller;


import com.company.bookseller.Model.User;
import com.company.bookseller.Service.IAuthenticationService;
import com.company.bookseller.Service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/authentication/")
public class AuthenticationController {

    @Autowired
    IAuthenticationService authenticationService;

    @Autowired
    IUserService userService;

    @PostMapping("sign-up")
    public ResponseEntity<User> signUp(@RequestBody User user)
    {

    }
}
