package com.company.bookseller.Model;

public enum Role {

    USER,
    ADMIN,
    SYSTEM_MANAGER
}
