package com.company.bookseller.Model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Table( name = "users")
@Entity
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column( name = "name", nullable = false, length = 100)
    private String name;

    @Column( name = "username", unique = true , nullable = false, length = 100)
    private String username;

    @Column( name = "password", nullable = false, length = 100)
    private String password;

    @Column( name = "created_at", nullable = false, length = 100)
    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    @Column( name = "role", nullable = false)
    private Role role;

    @Transient
    private String token ;




}
