package com.company.bookseller.Repository;

import com.company.bookseller.Model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase,Long> {

        @Query(" select " +
                "b.title as title , p.price as price,p.purchaseTime as purchaseTime"+
                " from Purchase p left join Book b on b.id = p.bookId + " +
                "where p.userId = :userId")
        List<Purchase> findAllPurchasesOfUser(@Param("userId") Long userId);
}
