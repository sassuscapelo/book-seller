package com.company.bookseller.Repository.Projection;

import java.time.LocalDateTime;

public interface PurchaseItem {

    String getTitle();
    Double getPrice();
    LocalDateTime getPurchaseTime();
}
