package com.company.bookseller.Service;


import com.company.bookseller.Model.Book;
import com.company.bookseller.Repository.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class BookService  implements IBookService{

    private final BookRepository bookRepository;

    @Override
    public Book saveBook(Book book)
    {
        book.setCreatedAt(LocalDateTime.now());
        return  bookRepository.save(book);
    }

    @Override
    public void deleteBook(Long id)
    {
        bookRepository.deleteById(id);
    }

    @Override
    public List<Book> findAllBooks()
    {
        return bookRepository.findAll();
    }
}
