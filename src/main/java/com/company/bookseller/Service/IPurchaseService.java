package com.company.bookseller.Service;

import com.company.bookseller.Model.Purchase;

import java.util.List;

public interface IPurchaseService {

    Purchase savePurchase(Purchase purchase);

    List<Purchase> findPurchasedItemsOfUser(Long userId);

}
