package com.company.bookseller.Service;

import com.company.bookseller.Model.Purchase;
import com.company.bookseller.Repository.PurchaseRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class PurchaseService implements IPurchaseService{


    private final PurchaseRepository purchaseRepository;

    @Override
    public Purchase savePurchase(Purchase purchase) {

        purchase.setPurchaseTime(LocalDateTime.now());
        return purchaseRepository.save(purchase);
    }

    @Override
    public List<Purchase> findPurchasedItemsOfUser(Long userId)
    {
        return purchaseRepository.findAllPurchasesOfUser(userId);
    }

}
